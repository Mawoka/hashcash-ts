import { mint, check } from ".";


describe("hashcash", () => {
    test("Mint", () => {
        const res_or = mint("a", 16, null, "", 8, true)
        const [version, claim, date, res, ext, rand, counter] = res_or.split(":")
        expect(version).toBe("1")
        expect(claim).toBe("16")
        expect(res).toBe("a")
        expect(ext).toBe("")
        expect(rand.length).toBe(8)
        const res_or2 = mint("a", 16, null, "", 8, false)
    })
    test("Check", () => {
        expect(check("1:20:220703:a::aktBfv1q:25b394", "a")).toBe(true)
        expect(check("3:20:220703:a::aktBfv1q:25b394", "a")).toBe(false)
        expect(check("1:20:220703:a::aktBfv1q", "a")).toBe(false)
        expect(check("1:a:220703:a::aktBfv1q:25b394", "a")).toBe(false)
        expect(check("1:20:220703:b::aktBfv1q:25b394", "a")).toBe(false)
        expect(check("1:16:220703:a::aktBfv1q:25b394", "a", 20)).toBe(false)
    })
})