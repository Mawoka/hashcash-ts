import { DateTime } from 'luxon'
import sha1 from "sha1"

const gen_salt = (l: number): string => {
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    const charLength = chars.length
    let result = ""
    for (let i = 0; i < l; i++) {
        result += chars.charAt(Math.floor(Math.random() * charLength))
    }
    return result
}

const hash_str_to_hex = (input: string): string => {
    return sha1(input)
}


export const mint = (resource: string, bits = 20, now = null, ext = "", saltchars = 8, stamp_seconds = false): string => {
    const ver = "1"
    let ts
    if (stamp_seconds) {
        ts = DateTime.now().toFormat("yyMMddHHmmss")
    } else {
        ts = DateTime.now().toFormat("yyMMdd")
    }
    const hex_digits = Math.ceil(bits / 4.)
    const zeros = "0".repeat(hex_digits)
    const salt = gen_salt(saltchars)
    // const salt = "125derte"
    const challenge = `${ver}:${bits}:${ts}:${resource}:${ext}:${salt}`
    let digest: string
    let counter = 0
    let result: string
    while (true) {
        digest = hash_str_to_hex(`${challenge}:${counter.toString(16)}`)
        if (digest.slice(0, hex_digits) == zeros) {
            result = counter.toString(16)
            break
        }
        counter += 1
    }
    return `${challenge}:${result}`
}


export const check = (stamp: string, resource: string, bits?: number): boolean => {
    if (!stamp.startsWith("1:")) {
        return false
    }
    const [version, claim, date, res, ext, rand, counter] = stamp.split(":")
    const isNotUndefined = (currentValue: string | number) => currentValue !== undefined;
    // console.log(version, claim, date, res, ext, rand, counter)
    if (![version, claim, date, res, ext, rand, counter].every(isNotUndefined)) {
        return false
    }
    if (isNaN(parseInt(claim))) {
        return false
    }
    if (res !== resource) {
        return false
    }

    if (bits !== undefined && bits > parseInt(claim)) {
        return false
    }

    const hex_digits = Math.floor(parseInt(claim) / 4.)


    const hash = hash_str_to_hex(stamp)
    return hash.startsWith("0".repeat(hex_digits))

}

// console.log(mint("a")) // 1:20:220703:a::aktBfv1q:25b394
// console.log(check("1:20:220703:a::aktBfv1q:25b394", "a"))

