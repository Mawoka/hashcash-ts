# HashCash

This is a fully working [Hashcash](http://hashcash.org)-implementation written in TypeScript.

The API is simple and more or less copied from the [Python implementation by David Mertz](http://hashcash.org/libs/python/). It doesn't have every feature, but the basic ones are present. This package also convinces you with a 100% test coverage.


## Basic usage
Look at the code or trust IntelliSense.